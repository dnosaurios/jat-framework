# JAT-Framework
Este repositorio contiene el framework donde se alojan las pruebas de aceptacion que [JATCodeApp ](https://bitbucket.org/dnosaurios/jatcodeapp)Genera, Jat-App es dependiente a este proyecto (sin este repositorio no funcionara).

# Pr-requisito
**1** Instala  Java + [variable de entorno seteada](https://docs.oracle.com/cd/E19182-01/820-7851/inst_cli_jdk_javahome_t/)

![JAVA_HOME.png](https://bitbucket.org/repo/gEqLzx/images/68165287-JAVA_HOME.png)

**2** instala Eclipse.

**3** Agrega [TestNG](http://toolsqa.com/selenium-webdriver/install-testng/) a Eclipse.

# Instalacion 
*Asegurate de tener los pre-requisito antes de proseguir*

**1** clona el repositorio ([Tienes que tener instalado git](https://git-scm.com/download)) *Unilizando http no SSH*

```git clone git@bitbucket.org:dnosaurios/jat-framework.git```

**2** Importa el proyecto en [Eclipse](http://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/neon3)

**3** Asegurate de Tener TestNG agreagado en el eclipse.

**4** una vez importado el proyecto has prueba de sanidad ejecutando sampleTest.java como TestNG hubicado en el directorio```arc.mf.client```


# Sanity Check
*Una vez configurado el entorno de ejecucion dode se generara los respectivos script de pruebas ha lo siguiente para asegurarnos que todo esta OK*

**1** Ejecuta la el fichero **SampleTest.java** seleccionando TestNG

![sampleTest.png](https://bitbucket.org/repo/gEqLzx/images/2767349228-sampleTest.png)

**2** Chrome deberia iniciarce especificando lo pasos que define **SampleTest.java**

