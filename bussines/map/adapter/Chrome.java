package map.adapter;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import map.target.Browser;

public class Chrome extends Browser{
	private WebDriver driver;
	
	public Chrome() {
		super();
		System.out.println("Chrome has been initialized");
	}

/* JATBUG-01 : https://bitbucket.org/dnosaurios/jatcodeapp/issues/19/jat-bug01
 *  Este metodo tiene que ser cambiado para leer
 *  el cromedriver de linux ademas de considerar el SO ubuntu
 *  
 * */
	@Override
	public void launchBrowser(WebDriver driver) {
		setDriver(driver);
//		System.getProperty("os.name").contains("Mac")?
		String SO=System.getProperty("os.name");
		if (SO.contains("Mac")){
			System.setProperty("webdriver.chrome.driver", "bussines/browsers/chromedriver");
		}else if (SO.contains("Win")){
			System.setProperty("webdriver.chrome.driver", "bussines/browsers/chromedriver.exe");
		}else if (SO.contains("Linux")){
			System.setProperty("webdriver.chrome.driver", "bussines/browsers/chromedriverln");
		}
		
		setDriver(((new ChromeDriver())));
		System.out.println("launching Chrome Browser");
	}

	@Override
	public void closeBrowser() {
		driver.close();
		System.out.println("Closing Chrome Browser");
		
	}

	@Override
	public WebDriver getDriver() {
		return driver;
	}

	@Override
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

}
